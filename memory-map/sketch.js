
const cellSize = 3;
const squareCellsPerRow = 256; 
const canvasWidth = cellSize * squareCellsPerRow;

const rectangleCanvasHeight = 64;
const mapBufferPixels = 32;


const memoryMap = [
  { start: 0x0000, end: 0x00ff, color: [255,   0,   0], type: 'ram', name: 'RAM - Zero page' },
  { start: 0x0100, end: 0x01ff, color: [255,   0, 200], type: 'ram', name: 'RAM - Stack' },
  { start: 0x0200, end: 0x3fff, color: [255, 200,   0], type: 'ram', name: 'RAM - Free Usable RAM' },
  { start: 0x4000, end: 0x400f, color: [  0, 255,   0], type: 'io',  name: 'IO 00' },
  { start: 0x4010, end: 0x401f, color: [  0, 255,   0], type: 'io',  name: 'IO 01' },
  { start: 0x4020, end: 0x402f, color: [  0, 255,   0], type: 'io',  name: 'IO 02' },
  { start: 0x4040, end: 0x404f, color: [  0, 255,   0], type: 'io',  name: 'IO 03' },
  { start: 0x4080, end: 0x408f, color: [  0, 255,   0], type: 'io',  name: 'IO 04' },
  { start: 0x4100, end: 0x410f, color: [  0, 255,   0], type: 'io',  name: 'IO 05' },
  { start: 0x4200, end: 0x420f, color: [  0, 255,   0], type: 'io',  name: 'IO 06' },
  { start: 0x4400, end: 0x440f, color: [  0, 255,   0], type: 'io',  name: 'IO 07' },
  { start: 0x4800, end: 0x480f, color: [  0, 255,   0], type: 'io',  name: 'IO 08' },
  { start: 0x5000, end: 0x500f, color: [  0, 255,   0], type: 'io',  name: 'IO 09 - ACIA for external UART' },
  { start: 0x6000, end: 0x600f, color: [  0, 255,   0], type: 'io',  name: 'IO 10 - VIA for LCD' },
  { start: 0x8000, end: 0xffff, color: [  0,   0, 255], type: 'rom', name: 'ROM' },
];

function setup() {
  const canvas = createCanvas(canvasWidth, rectangleCanvasHeight + mapBufferPixels + canvasWidth);
  background(0, 0);
  strokeWeight(0);
  pageWiseMap();
  translate(0, rectangleCanvasHeight + mapBufferPixels);
  squareMap();
  // put setup code here
}

function draw() {
  // put drawing code here
}

function pageWiseMap() {
  let memoryMapIndex = 0;
  for (let pageNum = 0; pageNum < 256; pageNum++) {
    const index = pageNum * 256;
    if (index > memoryMap[memoryMapIndex].end) {
      memoryMapIndex++;
    }

    if (index >= memoryMap[memoryMapIndex].start && index <= memoryMap[memoryMapIndex].end) {
      fill(...memoryMap[memoryMapIndex].color);
    } else {
      fill(128);
    }

    const x1 = pageNum * cellSize;
    const y1 = 0;
    const x2 = (pageNum + 1) * cellSize;
    const y2 = rectangleCanvasHeight;

    rect(x1, y1, x2, y2);
  }
}

function squareMap() {

  let memoryMapIndex = 0;

  for (let y = 0; y < squareCellsPerRow; y++) {
    for (let x = 0; x < squareCellsPerRow; x++) {
      const index = y * squareCellsPerRow + x;
      if (index > memoryMap[memoryMapIndex].end) {
        memoryMapIndex++;
      }

      if (index >= memoryMap[memoryMapIndex].start && index <= memoryMap[memoryMapIndex].end) {
        fill(...memoryMap[memoryMapIndex].color);
      } else {
        fill(128);
      }

      square(x * cellSize, y * cellSize, cellSize);
    }
  }
}