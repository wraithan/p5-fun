class Walker {
    constructor(pos, stuck) {
        this.pos = pos || this.makePos();
        this.stuck = stuck || false;
        this.color = 0;
    }

    makePos() {
        switch(floor(random(4))) {
            case 0: return createVector(random(width), 0);
            case 1: return createVector(random(width), height);
            case 2: return createVector(0, random(height));
            case 3: return createVector(width, random(height));
        }
    }

    walk() {
        this.pos.add(p5.Vector.random2D())
        this.pos.x = constrain(this.pos.x, 0, width);
        this.pos.y = constrain(this.pos.y, 0, height);
    }

    checkStuck() {
        for (const node of tree) {
            const dSq = distSq(this.pos, node.pos);
            if (dSq < r2) {
                if (random(1) < 0.1) {
                    this.stuck = true;
                    this.color = floor((found / maxFound) * 360);
                    break;
                }
            }
        }
    }

    show() {
        noStroke()
        if (!this.stuck) {
            const color = floor((found / maxFound) * 360);
            fill(color, 255, 255, 0.01);
        } else {
            fill(this.color, 255, 255, 0.5);
        }
        ellipse(this.pos.x, this.pos.y, r * 1.05, r * 1.05);
    }
}

function distSq (a, b) {
    var dx = b.x - a.x;
    var dy = b.y - a.y;
    return dx * dx + dy * dy;
}