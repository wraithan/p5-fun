const tree = [];
const r = 5;
const r2 = r * r;
const walkers = [];
const numWalkers = 100;
const iterations = 1000;
const maxFound = 50;
let found = 0;

function setup() {
    createCanvas(400, 400);
    colorMode(HSB);
    tree.push(new Walker(createVector(width / 2, height / 2), true));
    for (let i = 0; i < min(numWalkers, maxFound - found) - walkers.length; i++) {
        walkers.push(new Walker());
    }
}

function draw() {
    background(0);

    if (found >= maxFound) {
        for (let i = 0; i < walkers.length; i++) {
            walkers.pop();
        }
    } else {
        for (let i = 0; i < numWalkers - walkers.length; i++) {
            walkers.push(new Walker());
        }
    }

    for (let step = 0; step < iterations; step++) {
        for (let i = walkers.length - 1; i >= 0; i--) {
            let walker = walkers[i];
            walker.walk();
            walker.checkStuck();
            if (walker.stuck) {
                walkers.splice(i, 1);
                tree.push(walker);
                found++;
                console.log(found, walker.color);
                break;
            } else {
                walker.show()
            }
        }
    }

    for (const node of tree) {
        node.show();
    }
}