/// <reference path="../node_modules/@types/p5/global.d.ts" />

let controls
let lightMouse = false
let clearRender = false
let showInnerColumn = false
const vertialDistance = 800;
const maxHeight = vertialDistance / 2;
const numAgents = 4;
const agentImages = []

/** @type {Agent[]} */
const agents = [];

function setup() {
  const renderer = createCanvas(1024, 1024, WEBGL)
  document.getElementById('drawing').appendChild(renderer.canvas)
  controls = document.getElementById('controls')

  const lightControlBox = createCheckbox('Control Light', lightMouse);
  lightControlBox.changed(() => {
    lightMouse = lightControlBox.checked()
  });
  controls.appendChild(lightControlBox.elt);

  const columnBox = createCheckbox('Show Column', showInnerColumn);
  columnBox.changed(() => {
    showInnerColumn = columnBox.checked()
  });
  controls.appendChild(columnBox.elt);

  const clearBox = createCheckbox('Clear Between Renders', clearRender);
  clearBox.changed(() => {
    clearRender = clearBox.checked()
  });
  controls.appendChild(clearBox.elt);


  agentImages.push(buildPointImage(255, 0, 0))
  agentImages.push(buildPointImage(0, 255, 0))
  agentImages.push(buildPointImage(0, 0, 255))
  agentImages.push(buildPointImage(255, 255, 255))


  for (let i = 0; i < numAgents; i++) {
    agents.push(new Agent(100, 0.01, 1, i * HALF_PI, maxHeight, agentImages[i % agentImages.length]))
  }

  background(100);
}

function draw() {
  if (clearRender) {
    background(100)
  }

  // Scene Lighting
  ambientLight(200, 200, 200)

  // Light Obj
  createPointLight()

  for (const agent of agents) {
    agent.step();
    agent.draw();
  }

  // Inner Column
  if (showInnerColumn) {
    push()
    {
      noStroke()
      ambientMaterial(10, 200, 150)
      cylinder(75, vertialDistance)
      
      push()
      translate(225, 0, 0)
      cylinder(75, vertialDistance)
      pop()

      push()
      translate(-225, 0, 0)
      cylinder(75, vertialDistance)
      pop()
    }
    pop()
  }
}

function createPointLight() {
  if (!lightMouse) {
    return
  }
  let locX = mouseX - height / 2
  let locY = mouseY - width / 2
  pointLight(255, 255, 255, locX, locY, 0);
  push()
  {
    translate(locX, locY, 0);
    noStroke()
    ambientMaterial(255, 255, 255)
    sphere(5)
  }
  pop()
}

class Agent {
  /**
   * 
   * @param {number} r - Radius
   * @param {number} ra - Rotational Acceleration
   * @param {number} va - Vertical Acceralation
   * @param {number} it - Initial Theta
   * @param {number} ivp - Initial Vertial Position
   * @param {Image} image - texture to use for the agent
   */
  constructor(r = 100, ra = 0.01, va = 1, it = 0, ivp = 0, image) {
    this.radius = r
    this.theta = it;
    this.rotationalAcceleration = ra
    this.verticalPosition = ivp;
    this.verticalAcceleration = va
    this.image = image
    this.bounced = 0
  }

  getPos() {
    return [
      this.radius * cos(this.theta),
      this.verticalPosition,
      this.radius * sin(this.theta)
    ]
  }

  step() {
    this.theta += this.rotationalAcceleration;
    this.verticalPosition += this.verticalAcceleration
    const distancePast = Math.abs(this.verticalPosition) - maxHeight;
    if (distancePast > 0)  {
      if (this.verticalPosition > 0) {
        this.verticalPosition -= distancePast
      } else {
        this.verticalPosition += distancePast
      }
      this.bounced++;
      if (this.bounced > 10) {
        noLoop()
      }
      this.verticalAcceleration *= -1;
    }
  }

  draw() {
    const [locX, locY, locZ] = this.getPos();
    // pointLight(255, 255, 255, locX, locY, locZ);
    push()
    {
      translate(locX, locY, locZ);
      noStroke()
      texture(this.image)
      plane(15, 15)
    }
    pop()
  }
}

function buildPointImage(red, green, blue) {
  const pointImage = createImage(15, 15);
  pointImage.loadPixels();
  for (let i = 0; i < pointImage.width; i++) {
    for (let j = 0; j < pointImage.height; j++) {
      const vec = new p5.Vector(i - (pointImage.width / 2), j - (pointImage.height / 2));

      const mag = vec.mag()

      let val = 0
      if (mag < pointImage.width / 3) {
        val = (255 / mag) / 10
      }

      pointImage.set(i, j, color(red, green, blue, val));
    }
  }
  pointImage.updatePixels();
  return pointImage
}