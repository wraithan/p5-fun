let field;
const r = 100;
const k = 10;
const spacing = 40;
let position;
let rows, cols;
const rotationNoiseOffset = 0;
const rotationNoiseMultiplier = 0.01;
const angleNoiseOffset = 50;
const angleNoiseMultiplier = 0.01;
let time = 0;
const timeMultiplier = 0.001;
const numParticles = 10000;
const flowField = [];
const particles = [];
const timestep = 1;

function setup() {
    createCanvas(800, 800);
    background(0);
    
    rows = height / spacing;
    cols = width / spacing;
    for (let j = 0; j < rows; j++) {
        for (let i = 0; i < cols; i++) {
            let x = i * spacing + (spacing / 2);
            let y = j * spacing + (spacing / 2);
            flowField.push(new FlowVector(createVector(x, y)));
        }
    }
    for (let i = 0; i < numParticles; i++) {
        particles.push(new Particle());
    }
    // createLoop({
    //     duration: 2,
    //     framesPerSecond: 60,
    //     gif: {
    //         fileName: "dancing-flow-field.gif",
    //         workers: 10,
    //     }
    // })
}

function draw() {
    // background(0);

    for (let step = 0; step < timestep; step++) {
        for (let j = 0; j < rows; j++) {
            for (let i = 0; i < cols; i++) {
                const index = i + (j * rows);
                const flowVector = flowField[index];
                flowVector.update();
                // flowVector.show();
            }
        }

        for (const particle of particles) {
            const xCenter = Math.floor(particle.pos.x / spacing);
            const yCenter = Math.floor(particle.pos.y / spacing);
            
            let closestFV;
            let closestDistSq = Infinity;
            for (let xOffset = -1; xOffset <= 1; xOffset++) {
                for (let yOffset = -1; yOffset <= 1; yOffset++) {
                    let indexX = (xCenter + xOffset);
                    let indexY = (yCenter + yOffset);
                    if (indexX < 0 || indexX >= cols || indexY < 0 || indexY >= rows) continue;

                    const index = indexX + (indexY * rows);
                    const flowVector = flowField[index];
                    // console.log(index, indexX, indexY, rows, cols)
                    const dSq = distSq(flowVector.pos, particle.pos);
                    if (dSq < closestDistSq) {
                        closestDistSq = dSq;
                        closestFV = flowVector;
                    }
                }
            }
            if (closestFV) {
                particle.applyForce(closestFV.dir);
            }
            particle.update();
            particle.show();
        }
        time++;
    }
}


// function initAndDrawGrids() {
//     const gridA = generatePoissonGrid(width, height, r, k);
//     const gridB = generatePoissonGrid(width, height, r, k);

//     for (let i = 0; i < gridA.length; i++) {
//         const a = gridA[i];
//         const b = gridB[i];
//         stroke(255);
//         if (a !== undefined && b !== undefined) {
//             line(a.x, a.y, b.x, b.y);
//         }
//         if (a !== undefined) {
//             stroke(0, 255, 0);
//             point(a.x, a.y);
//         }
//         if (b !== undefined) {
//             stroke(0, 0, 255);
//             point(b.x, b.y);
//         }
//     }
// }

// function generatePoissonGrid(width, height, r, k) {
//     const r2 = r * r;
//     const w = Math.sqrt(r);
//     const cols = floor(width / w);
//     const rows = floor(height / w);
//     const active = [];
//     const grid = new Array(cols * rows);

//     const x = random(width);
//     const y = random(height);
//     const i = floor(x / w);
//     const j = floor(y / w);
//     const starter = createVector(x, y);
//     grid[i + j * cols] = starter;
//     active.push(starter);
    
//     while (active.length > 0) {
//         const ranIndex = floor(random(active.length));
//         const pos = active[ranIndex];
//         let found = false;
//         for (let n = 0; n < k; n++) {
//             const sample = p5.Vector.random2D();
//             const m = random(r, 2 * r);
//             sample.setMag(m);
//             sample.add(pos);
            
//             const col = floor(sample.x / w);
//             const row = floor(sample.y / w);
//             const gridIndex = col + row * cols;
//             if (col > -1 && row > -1 && col < cols && row < rows && !grid[gridIndex]) {
//                 let ok = true;
//                 for (let i = -1; i <= 1; i++) {
//                     for (let j = -1; i <= 1; i++) {
//                         const index = (col + i) + (row + j) * cols;
//                         const neighbor = grid[index];
//                         if (neighbor !== undefined) {
//                             const dSq = distSq(sample, neighbor);
//                             if (dSq < r2) {
//                                 ok = false;
//                             }
//                         }
//                     }
//                 }
                
//                 if (ok) {
//                     grid[gridIndex] = sample;
//                     active.push(sample);
//                     found = true;
//                 }
                
//             }
            
//         }
//         if (!found) {
//             active.splice(ranIndex, 1);
//         }
//     }
//     return grid;
// }

function distSq (a, b) {
    return a.x * a.x + b.y * b.y;
}