class FlowVector {
    constructor(pos) {
        this.startPos = pos;
        this.pos = this.startPos.copy();
        this.dir = null;
    }

    update() {
        const rotationNoiseX = (this.startPos.x + rotationNoiseOffset) * rotationNoiseMultiplier;
        const rotationNoiseY = (this.startPos.y + rotationNoiseOffset) * rotationNoiseMultiplier;
        let rotationAngle = noise(rotationNoiseX, rotationNoiseY, time * timeMultiplier);
        rotationAngle = map(rotationAngle, 0, 1, 0, PI * 8);

        const x = cos(rotationAngle) * spacing / 3;
        const y = sin(rotationAngle) * spacing / 3;
        this.pos.x = this.startPos.x + x;
        this.pos.y = this.startPos.y + y;

        const angleNoiseX = (this.pos.x + angleNoiseOffset) * angleNoiseMultiplier;
        const angleNoiseY = (this.pos.y + angleNoiseOffset) * angleNoiseMultiplier;
        let dirAngle = noise(angleNoiseX, angleNoiseY, time * timeMultiplier);
        dirAngle = map(dirAngle, 0, 1, 0, PI * 4);
        this.dir = p5.Vector.fromAngle(dirAngle, 0.1);
    }

    show() {
        noStroke()
        fill(0, 0, 255);
        ellipse(this.pos.x, this.pos.y, spacing / 8, spacing / 8);
        // stroke(255);
        // line(this.startPos.x, this.startPos.y, this.pos.x, this.pos.y);
        // fill(0, 255, 255);
        // ellipse(this.startPos.x, this.startPos.y, spacing / 8, spacing / 8)
        strokeWeight(1);
        stroke(0, 255, 0, Math.floor(256 * 0.8));
        line(this.pos.x, this.pos.y, this.pos.x + (this.dir.x * 100), this.pos.y + (this.dir.y * 100));
    }
}