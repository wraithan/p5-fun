const respawnType = 'side';

class Particle {
    constructor() {
        this.pos = createVector(0, 0);
        // this.vel = createVector(random(-1, 1), random(-1, 1)).mult(5);
        this.vel = createVector(0, 0);
        this.acc = createVector(0, 0);
        this.trail = [];
        this.last = null;
        const biggestSide = Math.max(width, height);
        this.maxAge = Math.floor(random(biggestSide/100, biggestSide/2));
        this.age = Math.floor(random(0, this.maxAge));
        this.spawn();
    }
    
    spawn() {
        this.last = null;
        this.age = 0;
        switch (respawnType) {
            case 'random':
            const value = createVector(random(width), random(height));
            this.vel.mult(0.1);
            // console.log('rando: ', value);
            this.pos = value;
            break;
            case 'side':
            switch (this.maxAge % 4) {
                case 0:
                    this.pos = createVector(random(width), 0);
                    break;
                case 1:
                    this.pos = createVector(random(width), height);
                    break;
                case 2:
                    this.pos = createVector(0, random(height));
                    break;
                case 3:
                    this.pos = createVector(width, random(height));
                    break;
                default: console.log('wut');
            }
            break;
            case 'mod frame':
            const multiplier = 4;
            const place = frameCount * multiplier;
            switch (this.maxAge % 4) {
                case 0:
                    this.pos = createVector(place % width, 0);
                    break;
                case 1:
                    this.pos = createVector(place % width, height);
                    break;
                case 2:
                    this.pos = createVector(0, place % height);
                    break;
                case 3:
                    this.pos = createVector(width, place % height);
                    break;
                default: console.log('wut');
            }
            break;
            default:
            console.log(`Wut: `, respawnType);
        }
    }
    
    update() {
        this.age += 1;
        this.last = this.pos.copy();
        this.vel.add(this.acc);
        this.vel.limit(3.0);
        // this.vel.rotate(random(-0.2, 0.2));
        this.pos.add(this.vel);
        this.acc.mult(0);
        let wrapped = (this.pos.x > width || this.pos.y > height || this.pos.x < 0 || this.pos.y < 0);
        
        if (wrapped || this.age > this.maxAge) {
            this.age = 0;
            this.spawn();
        }
    }
    
    applyForce(force) {
        this.acc.add(force);
        this.acc.mult(2.0);
    }
    
    show() {
        colorMode(HSB, 1);
        stroke(this.age / this.maxAge, 1, 1, 0.1);
        strokeWeight(1);
        if (this.last !== null) {
            line(this.pos.x, this.pos.y, this.last.x, this.pos.y);
        } else {
            point(this.pos.x, this.pos.y);
        }
        colorMode(RGB);
    }
}