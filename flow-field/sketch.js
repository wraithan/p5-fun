const field = [];
const particles = [];
let numParticles = 10000;
const spacing = 20;
let noiseOffset;
let angleNoiseScale = 0.008;
let angleNoiseSlider;
let magnitudeNoiseScale = 0.01;
let magnitudeNoiseSlider;
let noiseTimeScale = 0.005;
let noiseTimeSlider;
let noiseTime = 0;
let cols, rows;
let clearButton;
let showFlowCheckbox;
let showFlow = false;
let maxSpeedP;
let maxSpeedSlider;
let maxSpeed = 2.0;
let fpsP;
let respawnSelect;
let respawnType = 'random';
let respawnButton;
let xEffect = false;
let bgGrayScale = 0;
let currentStep = 0;

function setup() {
  createCanvas(800, 800);
  background(bgGrayScale);
  noiseOffset = 0// random(0.1, 0.8);
  cols = Math.floor(width / spacing);
  rows = Math.floor(height / spacing);
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      const noiseVector = createVector(i, j).rotate(PI/4);
      // const noiseI = noiseVector.x + noiseOffset;
      // const noiseJ = noiseVector.y + noiseOffset;
      const noiseI = i + noiseOffset;
      const noiseJ = j + noiseOffset;

      const angle = lerp(10*TWO_PI, 10*TWO_PI, noise(noiseI * angleNoiseScale, noiseJ * angleNoiseScale, noiseTime));
      // const angle = (noise(noiseI * angleNoiseScale, noiseJ * angleNoiseScale, noiseTime) * PI * 4) - TWO_PI;
      const magnitude = noise(noiseI * magnitudeNoiseScale, noiseJ * magnitudeNoiseScale, noiseTime);
      const flowVector = p5.Vector.fromAngle(angle, magnitude);

      field.push(flowVector);
    }
  }

  for (let i = 0; i < numParticles; i++) {
    particles.push(new Particle());
    // particles.push(new Particle(createVector(width/2, height)));
    // particles.push(new Particle(createVector(random(width), random(height))))
    // switch (i % 4) {
    //   case 0:
    //       particles.push(new Particle(createVector(random(width), 0)));
    //     break;
    //   case 1:
    //       particles.push(new Particle(createVector(random(width), height)));
    //     break;
    //   case 2:
    //       particles.push(new Particle(createVector(0, random(height))));
    //     break;
    //   case 3:
    //       particles.push(new Particle(createVector(width, random(height))));
    //     break;
    //   default: console.log('wut');
    // }
  }
  clearButton = createButton('Clear');
  clearButton.mouseClicked(clearBackground);
  showFlowCheckbox = createCheckbox('Show Flow', showFlow);
  showFlowCheckbox.changed(showFlowChanged);
  fpsP = createP();
  maxSpeedP = createP('Max Speed')
  maxSpeedSlider = createSlider(0.0001, 100, maxSpeed, 0.0001);
  respawnSelect = createSelect();
  respawnSelect.option('random');
  respawnSelect.option('side');
  respawnSelect.option('mod frame');
  respawnSelect.changed(respawnSelectChanged);
  respawnButton = createButton('Respawn');
  respawnButton.mouseClicked(() => {
    currentStep = 0;
    clearBackground();
    for (const particle of particles) {
      particle.spawn();
    }
  })
  angleNoiseSlider = createSlider(0.0001, 0.1, angleNoiseScale, 0.0001);
  magnitudeNoiseSlider = createSlider(0.0001, 0.1, magnitudeNoiseScale, 0.0001);
  noiseTimeSlider = createSlider(0.0001, 0.1, noiseTimeScale, 0.0001);
  // frameRate(5);
}

function clearBackground() {
  background(bgGrayScale);
}

function showFlowChanged() {
  showFlow = this.checked();
  background(bgGrayScale);
}

function respawnSelectChanged() {
  respawnType = respawnSelect.value();
}
function mouseDragged() {
  // particles.push(new Particle(createVector(mouseX, mouseY)));
}

function draw() {
  maxSpeed = maxSpeedSlider.value();
  maxSpeedP.html(`Max Speed: ${maxSpeed}`);
  angleNoiseScale = angleNoiseSlider.value();
  magnitudeNoiseScale = magnitudeNoiseSlider.value();
  noiseTimeScale = noiseTimeSlider.value();

  const scaling = spacing * 0.7;
  noiseTime = currentStep * noiseTimeScale;
  if (showFlow) {
    background(0);
  }

  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      let magnitude = noise(i * magnitudeNoiseScale, j * magnitudeNoiseScale, noiseTime);
      const index = (j * rows) + i;
      let angle = lerp(-TWO_PI, TWO_PI, noise((i + noiseOffset) * angleNoiseScale, (j + noiseOffset) * angleNoiseScale, noiseTime));
      // if (j === Math.floor(rows/2)) {
      //   angle = PI/2;
      //   magnitude *= 0.1;
      // } else if (j === Math.floor(rows/2) - 1) {
      //   angle = -PI/2;
      //   magnitude *= 0.1;
      if (xEffect) {
        if (i === j) {
          angle = -HALF_PI;
          magnitude *= 2;
        } else if (i === rows-j) {
          angle = HALF_PI;
          magnitude *= 2;
        } else {
          magnitude *= 0.1;
        }
      }
      field[index] = p5.Vector.fromAngle(angle, magnitude);
    }
  }

  if (showFlow) {
    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        push();
        stroke(255);
        translate(i * spacing, j * spacing);
        const index = j * rows + i;
        const flowVector = field[index];
        line(0, 0, flowVector.x * scaling, flowVector.y * scaling);
        noStroke()
        fill(200, 100, 150);
        ellipse(0, 0, 1)
        pop();
      }
    }
  }
  for (const particle of particles) {
    const flowIndex = Math.floor(particle.pos.x / spacing) + Math.floor((particle.pos.y / spacing)) * rows;
    particle.applyForce(field[flowIndex]);
    particle.update()
    particle.show();
  }
  if (currentStep % 10 === 0) {
    noStroke()
    fill(bgGrayScale, bgGrayScale, bgGrayScale, 3);
    rect(0, 0, width, height);
  }
  // if (currentStep % 1000 === 0) {
  //   noStroke()
  //   fill(0, 0, 0);
  //   rect(0, 0, width, height);
  // } 
  // noLoop()
  fpsP.html(`Frame Count: ${currentStep}, FPS: ${frameRate()}`);
  // console.log(particles.length, currentStep, frameRate());

  currentStep++;
}

class Particle {
  constructor() {
    this.pos = createVector(0, 0);
    // this.vel = createVector(random(-1, 1), random(-1, 1)).mult(5);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.trail = [];
    this.last = null;
    const biggestSide = Math.max(width, height);
    this.maxAge = Math.floor(random(biggestSide/100, biggestSide/2));
    this.age = Math.floor(random(0, this.maxAge));
    this.spawn();
  }

  spawn() {
    this.last = null;
    this.age = 0;
    switch (respawnType) {
      case 'random':
          const value = createVector(random(width), random(height));
          this.vel.mult(0.1);
          // console.log('rando: ', value);
          this.pos = value;
        break;
      case 'side':
        switch (this.maxAge % 4) {
          case 0:
              this.pos = createVector(random(width), 0);
            break;
          case 1:
              this.pos = createVector(random(width), height);
            break;
          case 2:
              this.pos = createVector(0, random(height));
            break;
          case 3:
              this.pos = createVector(width, random(height));
            break;
          default: console.log('wut');
        }
        break;
      case 'mod frame':
          const multiplier = 4;
          const place = currentStep * multiplier;
          switch (this.maxAge % 4) {
            case 0:
                this.pos = createVector(place % width, 0);
              break;
            case 1:
                this.pos = createVector(place % width, height);
              break;
            case 2:
                this.pos = createVector(0, place % height);
              break;
            case 3:
                this.pos = createVector(width, place % height);
              break;
            default: console.log('wut');
          }
        break;
      default:
        console.log(`Wut: `, respawnType);
    }
  }

  update() {
    this.age += 1;
    this.last = this.pos.copy();
    this.vel.add(this.acc);
    this.vel.limit(3.0);
    this.vel.rotate(random(-0.2, 0.2));
    this.pos.add(this.vel);
    this.acc.mult(0);
    let wrapped = (this.pos.x > width || this.pos.y > height || this.pos.x < 0 || this.pos.y < 0);
    if (this.pos.x > width) {
      wrapped = true;
      // this.pos.x = 0;
      // this.pos.y = random(height);
    } else if (this.pos.x < 0) {
      wrapped = true;
      // this.pos.x = width;
      // this.pos.y = random(height);
    }

    if (this.pos.y > height) {
      wrapped = true;
      // this.pos.y = 0;
      // this.pos.x = random(width);
    } else if (this.pos.y < 0) {
      wrapped = true;
      // this.pos.y = height;
      // this.pos.x = random(width);
    }


    if (wrapped || this.age > this.maxAge) {
      // this.vel.mult(-10);
      // this.pos = createVector(mouseX, mouseY);
      // this.last = null;
      // this.pos = createVector(random(width), random(height));
      // this.vel.mult(0);
      // this.vel.mult(-1);
      this.age = 0;
      this.spawn();
    }
    // this.trail.push(this.pos.copy());
  }

  applyForce(force) {
    this.acc.add(force);
    this.acc.mult(2.0);
  }

  show() {
    // noStroke();
    // stroke(0, 20, currentStep % 256, 5);
    // const red = this.vel.mag() * 128;
    // stroke(red, Math.min(abs(((currentStep + 256) % 512) - 256), red * 0.8), 20, (this.age / this.maxAge) * 10);
    colorMode(HSB);
    stroke(Math.floor((this.age / this.maxAge) * 256), 255, 255, 0.1);
    // if (currentStep % 256 === 0) {
    //   console.log('reset');
    // }
    strokeWeight(1);
    if (this.last !== null) {
      line(this.pos.x, this.pos.y, this.last.x, this.pos.y);
    }
    colorMode(RGB);
    // for (const t of this.trail) {
    //   ellipse(t.x, t.y, 3);
    // }
    // fill(255);
    // const t = this.trail[this.trail.length - 1];
    // ellipse(this.pos.x, this.pos.y, 3);
  }
}