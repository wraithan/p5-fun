const numPoints = 100;
const numLayers = 10;


function setup() {
    createCanvas(800, 800);
    background(41)
    colorMode(HSB);
}

function draw() {
    noStroke();
    for (let i = 0; i < numPoints; i++) {
        for (let layer = 1; layer <= numLayers; layer++) {
            const pos = layerRandom(layer);
            fill((50.0 / numLayers) * layer, 255, 255, 1/numLayers);
            ellipse(pos.x, pos.y, 2, 2);
        }
    }
}

function pureRandom() {
    return createVector(random(width), random(height));
}

function layerRandom(num) {
    const pos = createVector(0, 0);
    for (var i = 0; i < num; i++) {
        pos.x += random(width);
        pos.y += random(height);
    }
    pos.x /= num;
    pos.y /= num;
    return pos;
}