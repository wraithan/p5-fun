const r = 7;
const r2 = r * r;
const k = 5; // Attempts
const grid = [];
// const active = [];
// const ordered = [];
const w = r / Math.sqrt(2);
let cols, rows;
const steps = 1000;
/** @type {Seed[]} */
const seedPoints = [];

const choiceAlgorithms = [
  'start',
  'start',
  'start',
  'firstNEnd',
  // 'end',
  'random',
  'random',
  'random',
]


class Seed {
  constructor(choiceAlgo, hueStart, choiceInput) {
    this.ordered = [];
    this.active = [];
    this.choiceAlgo = choiceAlgo;
    if (!choiceAlgo) {
      this.choiceAlgo = random(choiceAlgorithms);
    }
    this.choiceInput = choiceInput;
    if (!choiceInput) {
      this.choiceInput = random(0, 10_000);
    }
    this.hueStart = hueStart;

    const x = random(width);
    const y = random(height);
    this.pos = createVector(x, y);
    this.addPoint(this.pos);
  }

  chooseIndex() {
    let choice = 0;
    switch (this.choiceAlgo) {
      case 'start':
        choice = 0;
        break;
      case 'end':
        choice = this.active.length - 1;
        break;
      case 'random':
        choice = random(0, this.active.length - 1);
        break;
      case 'firstNEnd':
        if (this.ordered.length < this.choiceInput) {
          choice = this.active.length - 1;
        } else {
          choice = 0;
        }
    }
    return Math.floor(choice)
    // let ranIndex = floor(random(this.active.length));
    // const ranIndex = 0;
    // let ranIndex = this.active.length - 1;
    // const divisor = 100;
    // const choice = random(this.active.length / divisor);
    // let ranIndex = choice;
    // if (this.active.length < 1_000) {
    // let ranIndex = 0;
    // } else 
    // if (this.active.length % 30 === 0) {
    //   ranIndex = this.active.length - 1;
    // }
    // } else if (this.active.length > 5_000) {
    //   ranIndex = 0;
    // }
    // ranIndex = floor(ranIndex);
  }

  grow(maxSteps) {
    for (let stepper = 0; stepper < maxSteps; stepper++) {
      if (this.active.length > 0) {
        const ranIndex = this.chooseIndex();
        const pos = this.active[ranIndex];
        let found = false;
        for (let n = 0; n < k; n++) {
          const sample = p5.Vector.random2D();
          const m = random(r, 2*r);
          sample.setMag(m);
          sample.add(pos);
  
          const col = floor(sample.x / w);
          const row = floor(sample.y / w);
  
          if (col > -1 && row > -1 && col < cols && row < rows && !grid[col + row * cols]) {
            let ok = true;
            for (let i = -1; i <= 1; i++) {
              for (let j = -1; i <= 1; i++) {
                const index = (col + i) + (row + j) * cols;
                const neighbor = grid[index];
                if (neighbor !== undefined) {
                  const dSq = distSq(sample, neighbor);
                  if (dSq < r2) {
                    ok = false;
                  }
                }
              }
            }
  
            if (ok) {
              this.addPoint(sample);
              found = true;
              continue;
            }
  
          }
  
        }
        if (!found) {
          this.active.splice(ranIndex, 1);
        }
      }
    }
  }

  addPoint(pos) {
    const i = floor(pos.x / w);
    const j = floor(pos.y / w);
    grid[i + j * cols] = pos;
    this.active.push(pos);
    this.ordered.push(pos);
  }

  show() {
    let furthestDistSq = 0;
    for (let i = 0; i < this.ordered.length; i++) {
      let pos = this.ordered[i];
      if (pos !== undefined) {
        let distance = this.pos.dist(pos);
        if (distance > furthestDistSq) {
          furthestDistSq = distance;
        }
      }
    }
    for (let i = 0; i < this.ordered.length; i++) {
      let pos = this.ordered[i];
      if (this.ordered[i] !== undefined) {
        colorMode(HSB, 1);
        // let hue = (((i / this.ordered.length) * 16) % 16) / 16 ;
        
        let distance = this.pos.dist(pos);
        let hue = this.hueStart + ((distance / furthestDistSq) * 0.20);
        stroke(hue, 1, 1);
        strokeWeight(r * 0.3);
        point(pos.x, pos.y);
        colorMode(RGB);
      }
    }
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(144);

  // Step 0
  cols = floor(width / w);
  rows = floor(height / w);
  console.log({cols, rows});

  for (let i = 0; i < cols * rows; i++) {
    grid[i] = undefined;
  }

  const seeds = rows / 10;
  for (let i = 0; i < seeds; i++) {
    seedPoints.push(new Seed('firstNEnd', i / seeds, 500));
  }
  // seedPoints.push(new Seed('start'));
  // seedPoints.push(new Seed('end'));
  // seedPoints.push(new Seed('random'));
}

function draw() {
  // background(0);
  // let maxSteps = Math.min(Math.max(1, ordered.length / 100), steps);
  // let maxSteps = Math.max(10, seedPoint.active.length / 2);
  let maxSteps = 100;
  // let maxSteps = steps;
 
  let numActive = 0
  let numOrdered = 0;
  for (let i = 0; i < maxSteps; i++) {
    for (let seedPoint of seedPoints) {
      if (i > seedPoint.active.length) {
        continue;
      }
      seedPoint.grow(1)
    }
  }
  
  for (let seedPoint of seedPoints) {
    numActive += seedPoint.active.length;
    numOrdered += seedPoint.ordered.length;
    seedPoint.show();
  }

  // for (const activePos of active) {
  //   stroke(255);
  //   strokeWeight(r * 0.1);
  //   point(activePos.x, activePos.y);
  // }
  
  document.title = `Poisson (${numActive}, ${numOrdered}) [${maxSteps}]`;
  if (numActive === 0) {
    noLoop();
  }
}

function distSq (a, b) {
  return (a.x * a.x) + (b.y * b.y);
}