const fs = require('fs');

const projects = [];
const currentDir = fs.readdirSync('./');
for (const entry of currentDir) {
    if (entry.startsWith('.')) {
        continue;
    }
    const stat = fs.statSync(entry);
    if (stat.isDirectory()) {
        projects.push(entry);
    }
}

const base = fs.readFileSync('./base-index.html', 'utf8');

const lines = base.split('\n');

const index = lines.flatMap(line => {
    const content = line.trim();
    if (content === '<!-- Links -->') {
        return projects.map(proj => `<li><a href="./${proj}/index.html">${proj}</a></li>`);
        
    } else {
        return line;
    }
}).join('\n');

fs.writeFileSync('./index.html', index);

