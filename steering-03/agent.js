class Agent {
    constructor(color) {
        this.pos = createVector(random(width), random(height));
        this.vel = createVector(0, 0);
        this.acc = createVector(0, 0);
        this.last = this.pos.copy();
        this.origin = this.pos.copy();
        this.color = color;
        this.maxSpeed = 2;
        this.maxForce = 0.01;
    }

    update() {
        this.last = this.pos.copy();
        this.vel.add(this.acc);
        this.pos.add(this.vel);
        this.acc.mult(0);
    }

    addForce(force) {
        this.acc.add(force);
    }

    seek(target) {
        const steering = target.copy();
        steering.sub(this.pos);
        let m = map(this.pos.dist(target), 0, 50, 0, this.maxSpeed);
        steering.setMag(m);
        steering.sub(this.vel);
        steering.limit(this.maxForce);
        this.addForce(steering);
    }

    show(alpha) {
        colorMode(HSB)
        strokeWeight(3);
        stroke(this.color, 255, 255, alpha);
        line(this.pos.x, this.pos.y, this.last.x, this.last.y);
        strokeWeight(1);
        stroke(this.color, 255, 255, alpha * 0.01);
        line(this.pos.x, this.pos.y, this.origin.x, this.origin.y);

        colorMode(RGB)
    }
}