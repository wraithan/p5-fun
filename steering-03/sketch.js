
const numAgents = 5;
const agents = []
const numTargets = 100;
const targets = [];
let targetIndex = 0;
const iterations = 3;
let offset = 0;

function setup() {
  createCanvas(800, 800);
  background(0);

  for (let i = 0; i < numTargets; i++) {
    targets.push(createVector(random(width), random(height)))
  }

  for (let i = 0; i < numAgents; i++) {
    const color = Math.floor(random(256));
    agents.push(new Agent(color));
  }
}

function draw() {
  if (frameCount < 2) {
    background(0);
  }
  noStroke()
  for (let i = 0; i < numTargets; i++) {
    // const target = targets[i];
    // fill(128 + ((128 / numTargets) * i), 0, 0);
    // ellipse(target.x, target.y, 5, 5);
  }

  for (let i = 0; i < iterations; i++) {
    const agent = agents[targetIndex % numAgents];
  
    const relativeIndex = (targetIndex + (offset * (targetIndex % 2))) % numTargets;
    const currentTarget = targets[relativeIndex];

    agent.seek(currentTarget);
    agent.update();
    agent.show(0.1);
    if (agent.pos.dist(currentTarget) < 2) {
      targetIndex++;
      if (targetIndex == numTargets) {
        // noLoop()
        // console.log('Done');
        // return;
        targetIndex = targetIndex % numTargets;
        offset++;
      }
    }
  }
  // console.log(relativeIndex, numTargets);
}