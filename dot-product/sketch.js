let wall;

function setup() {
    createCanvas(500, 500);
    wall = new Wall(random(width), random(height), random(width), random(height));
}

function draw() {
    background(0);

    stroke(255);
    strokeWeight(1)
    fill(0)
    ellipse(mouseX, mouseY, 15, 15);

    stroke(255, 255, 255, 64);
    strokeWeight(1)
    line(mouseX, mouseY, wall.pos1.x, wall.pos1.y);

    wall.show();
}

class Wall {
    constructor(x1, y1, x2, y2) {
        this.pos1 = createVector(x1, y1);
        this.pos2 = createVector(x2, y2);
    }

    show() {
        stroke(255, 255, 255, 128);
        strokeWeight(2)
        line(this.pos1.x, this.pos1.y, this.pos2.x, this.pos2.y);
    }
}