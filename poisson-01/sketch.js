const r = 7;
const r2 = r * r;
const k = 5;
const grid = [];
const active = [];
const ordered = [];
const w = r / Math.sqrt(2);
let cols, rows;
const steps = 100;

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  
  cols = floor(width / w);
  rows = floor(height / w);
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(144);

  // Step 0
  cols = floor(width / w);
  rows = floor(height / w);
  console.log({cols, rows});

  for (let i = 0; i < cols * rows; i++) {
    grid[i] = undefined;
  }

  // Step 1
  const x = random(width);
  const y = random(height);
  const i = floor(x / w);
  const j = floor(y / w);
  const pos = createVector(x, y);
  grid[i + j * cols] = pos;
  active.push(pos);
  console.log('starter: ', pos);
}

function draw() {
  background(0);
  let maxSteps = Math.min(Math.max(1, ordered.length / 100), steps);
  // let maxSteps = steps;
  document.title = `Poisson (${active.length}, ${ordered.length}) [${maxSteps}]`;
  for (let stepper = 0; stepper < maxSteps; stepper++) {
    if (active.length > 0) {
      // const ranIndex = floor(random(active.length));
      // const ranIndex = 0;
      // const ranIndex = active.length - 1;
      const divisor = 100;
      const choice = random(active.length / divisor);
      let ranIndex = choice;
      if (active.length < 10_000) {
        ranIndex = active.length - choice;
      }
      ranIndex = floor(ranIndex);
      const pos = active[ranIndex];
      let found = false;
      for (let n = 0; n < k; n++) {
        const sample = p5.Vector.random2D();
        const m = random(r, 2*r);
        sample.setMag(m);
        sample.add(pos);

        const col = floor(sample.x / w);
        const row = floor(sample.y / w);

        if (col > -1 && row > -1 && col < cols && row < rows && !grid[col + row * cols]) {
          let ok = true;
          for (let i = -1; i <= 1; i++) {
            for (let j = -1; i <= 1; i++) {
              const index = (col + i) + (row + j) * cols;
              const neighbor = grid[index];
              if (neighbor !== undefined) {
                const dSq = distSq(sample, neighbor);
                if (dSq < r2) {
                  ok = false;
                }
              }
            }
          }

          if (ok) {
            grid[col + row*cols] = sample;
            active.push(sample);
            ordered.push(sample);
            found = true;
          }

        }

      }
      if (!found) {
        active.splice(ranIndex, 1);
      }
    }
  }

  for (var i = 0; i < ordered.length; i++) {
    if (ordered[i] !== undefined) {
      colorMode(HSB, 1);
      let hue = (((i / ordered.length) * 16) % 16) / 16 ;
      stroke(hue, 1, 1);
      strokeWeight(r * 0.3);
      point(ordered[i].x, ordered[i].y);
      colorMode(RGB);
    }
  }

  for (const activePos of active) {
    stroke(255);
    strokeWeight(r * 0.1);
    point(activePos.x, activePos.y);
  }
}

function distSq (a, b) {
  return a.x * a.x + b.y * b.y;
}