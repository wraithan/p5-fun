/// <reference path="../node_modules/@types/p5/global.d.ts" />

let sun;
let biggestSide;

function setup() {
  createCanvas(600, 600);
  biggestSide = max(height, width);
  biggestSideSq = biggestSide * biggestSide;
  sun = new Body({radius: 40});
  sun.spawnMoons(3);
  background(0);
}

function draw() {
  // background(0);
  translate(width/2, height/2);
  sun.show(2);
  sun.orbit();
}

class Body {
  constructor({radius = 0, distance = 0, angle = 0, orbitSpeed = 0 }) {
    console.log(`That's no moon: ${radius}, ${distance}, ${angle}`)
    this.radius = radius;
    this.distance = distance;
    this.angle = angle;
    this.satelites = [];
    this.orbitSpeed = orbitSpeed || random(0.01);
    this.hue = random(0, 360)
  }

  spawnMoons(num = 1, maxDistance = 300) {
    for (let i = 0; i < num; i++) {
      const radius = random(this.radius * 0.1, this.radius * 0.5);
      const distance = random(this.radius + radius, maxDistance);
      const angle = random(TWO_PI);
      const body = new Body({radius, distance, angle});
      body.spawnMoons(Math.floor(num * random()), radius + (distance * 0.5))
      this.satelites.push(body);
    }
  }

  orbit() {
    this.angle += this.orbitSpeed;
    for (const satelite of this.satelites) {
      satelite.orbit();
    }
  }

  /**
   * 
   * @param {p5} p 
   * @param {*} showLevel 
   * @param {*} currentLevel 
   */
  show(p, showLevel, currentLevel = 0 ) {
    push()
    rotate(this.angle)
    translate(this.distance, 0);
    // fill(255, 100)
    // ellipse(0, 0, this.radius * 2, this.radius * 1.95);
    if (currentLevel >= showLevel) {
      colorMode(HSB);
      stroke(this.hue, 50, 50, 1);
      point(0, 0);
    }
    for (const satelite of this.satelites) {
      satelite.show(showLevel, currentLevel + 1);
    }
    pop()
  }
}