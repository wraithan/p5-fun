let numPoints = 9999;
let currentIndex = 0;
let multiplier = 249;
const colorInterval = Math.ceil(numPoints / (multiplier - 1));
let radius;
let originX;
let originY;
const steps = Math.ceil(colorInterval * 8);
const loops = 10;
const alpha = 64 / loops;

function setup() {
  createCanvas(800, 800);
  background(0);
  originX = floor(width / 2);
  originY = floor(height / 2);
  radius = width * 0.5 * 0.9;
  strokeWeight(1)
  /*
  translate(originX, originY);
  noStroke()
  fill(255);
  ellipse(0, 0, 3, 3);

  fill(0, 200, 150);
  for (let i = 0; i < numPoints; i++) {
    const point = pointFromI(i);
    ellipse(point.x, point.y, 3, 3);
  }
  */
  // frameRate(60);
}

function draw() {
  translate(originX, originY);
  for (let step = 0; step < steps; step++) {
    currentIndex++;
    let currentPoint = pointFromI(currentIndex)
    let nextPoint = pointFromI(currentIndex * multiplier);
    let colorIndex = floor(currentIndex / colorInterval);
    // stroke(230, 60, 10, alpha); // red orange
    // stroke(200, 170, 30, alpha); // yellow
    // stroke(250, 20, 20, alpha); // redder
    // stroke(40, 190, 60, alpha) // green
    // stroke(10, 60, 190, alpha) // blue
    switch (colorIndex % loops) {
      case 0:
      case 2:
        stroke(40, 190, 60, alpha / 2) // green
        break;
      case 1:
        stroke(10, 60, 190, 0) // blue
        break;
      case 3:
        stroke(200, 170, 30, alpha); // yellow
        break;
    }
    line(currentPoint.x, currentPoint.y, nextPoint.x, nextPoint.y);
    if (currentIndex === numPoints * (loops / 2)) {
      noLoop();
      break
    }
  }
}

function pointFromI(i) {
  const angle = i * (TWO_PI / numPoints);
  return {
    x: radius * cos(angle),
    y: radius * sin(angle)
  }
}