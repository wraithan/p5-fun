/// <reference path="../node_modules/@types/p5/global.d.ts" />

const NO_SCROLLBAR_BUFFER = 5;
const DIFFUSAL = 0.95;
const NUM_LIGHTBARS = 200;
const NUM_LIGHTS = 75;
const LIGHT_SPREAD = 8;
const FREQUENCY = 30;
const lightBars = new Array(NUM_LIGHTBARS);

function setup() {
  createCanvas(windowWidth - NO_SCROLLBAR_BUFFER, windowHeight - NO_SCROLLBAR_BUFFER);
  // frameRate(1);
  for (let i = 0; i < lightBars.length; i++) {
    lightBars[i] = new LightBar(NUM_LIGHTS);
  }
}

function windowResized() {
  resizeCanvas(windowWidth - NO_SCROLLBAR_BUFFER, windowHeight - NO_SCROLLBAR_BUFFER);
}
// let loopCount = 0;
let lastDiffuse = 0;
function draw() {
  // loopCount++;
  // if (loopCount > 10) {
  //   noLoop();
  // }
  
  background(40);
  const now = millis();
  let doDiffuse = now - lastDiffuse > 100;
  if (doDiffuse) {
    lastDiffuse = now;
  }
  const barInterval = Math.max(22, height / lightBars.length);
  for (let i = 0; i < lightBars.length; i++) {
    const lightBar = lightBars[i];
    if (doDiffuse) {
      lightBar.maybeLight();
      lightBar.diffuse();
    }
    push()
    translate(0, barInterval * i);
    lightBar.draw(barInterval - 2);
    pop()
  }
}

class LightBar {
  /**
   * 
   * @param {number} lights 
   */
  constructor(lights) {
    /** @type {number[]} */
    this.lights = new Array(lights);
    for (let i = 0; i < lights; i++) {
      this.lights[i] = 0;
    }

    this.baseColor = color(30);
    this.litColor = color(60, 125, 240);
  }

  getColorForLight(light) {
    // console.log(this.lights);
    return lerpColor(this.baseColor, this.litColor, this.lights[light]);
  }

  maybeLight() {
    const rand = random(0, FREQUENCY * this.lights.length);
    if (rand < this.lights.length) {
      console.log('ping')
      this.lights[Math.floor(rand)] = 1.0;
    }
  }

  diffuse() {
    const newLights = new Array(this.lights.length);
    const oldLights = this.lights;
    for (let i = 0; i < newLights.length; i++) {
      let total = 0;
      let count = 0;
      for (let offset = -LIGHT_SPREAD; offset < LIGHT_SPREAD; offset++ ) {
        const index = i + offset;
        if (index < 0 || index >= newLights.length) {
          continue;
        }
        let multi = (LIGHT_SPREAD + 1) - Math.abs(offset);
        multi *= multi;
        // console.log({total, count, index, multi, offset});
        total += oldLights[index] * multi;
        count += multi;
      }
      newLights[i] = (total / count) * DIFFUSAL;
      // if (i == 0) {
      //   newLights[i] = (((oldLights[i] * 2.0) + oldLights[i + 1]) / 3.0) * DIFFUSAL;
      // } else if (i == newLights.length - 1) {
      //   newLights[i] = ((oldLights[i - 1] + (oldLights[i] * 2.0)) / 3.0) * DIFFUSAL;
      // } else {
      //   newLights[i] = ((oldLights[i - 1] + (oldLights[i] * 2.0) + oldLights[i + 1]) / 4.0) * DIFFUSAL;        
      // }
    }
    this.lights = newLights;
  }

  draw(barInterval) {
    const base = width / this.lights.length;
    strokeWeight(2);
    for (let i = 0; i < this.lights.length; i++) {
      let c = this.getColorForLight(i);
      fill(c);
      rect(base * i, 0, base, barInterval);
    }
  }
}