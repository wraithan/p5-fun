'use strict'

const field = []
const particles = []
const numParticles = 5000
const spacing = 40
let noiseOffset
let angleNoiseScale = 0.008
let magnitudeNoiseScale = 0.01
let noiseTimeScale = 0.005
let noiseTime = 0
let cols, rows
let showFlow = false
let maxSpeed = 2.0
let respawnSelect
let respawnType = 'mod frame'
let respawnButton
const xEffect = false
const bgGrayScale = 0
let currentStep = 0

Vue.component('config-param', {
  props: ['param'],
  template: `<div>
  <label>{{ param.name }}
    <template v-if="param.type === 'range'">
      <input
        v-model="param.value"
        v-bind:type="param.type"
        v-bind:min="param.extra.min"
        v-bind:max="param.extra.max"
        v-bind:step="param.extra.step">
        {{ param.value }}
      </input>
    </template>
    <template v-else>
      <input
        v-model="param.value"
        v-bind:type="param.type">
      </input>
    </template>
  </label>
</div>`
})

class FlowField {
  constructor () {
    this.sketch = undefined
    this.configVue = undefined
  }

  init (sketch) {
    this.sketch = sketch

    this.sketch.setup = this.setup.bind(this)
    this.sketch.draw = this.draw.bind(this)
  }

  setup () {
    this.setupUI()

    const canvas = this.sketch.createCanvas(800, 800)
    canvas.parent(document.getElementById('drawing'))
    this.sketch.background(bgGrayScale)
    noiseOffset = 0// random(0.1, 0.8);
    cols = Math.floor(this.sketch.width / spacing)
    rows = Math.floor(this.sketch.height / spacing)
    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        // const noiseVector = this.sketch.createVector(i, j).rotate(this.sketch.PI/4);
        // const noiseI = noiseVector.x + noiseOffset;
        // const noiseJ = noiseVector.y + noiseOffset;
        const noiseI = i + noiseOffset
        const noiseJ = j + noiseOffset

        const angle = this.sketch.lerp(10 * this.sketch.TWO_PI, 10 * this.sketch.TWO_PI, this.sketch.noise(noiseI * angleNoiseScale, noiseJ * angleNoiseScale, noiseTime))
        // const angle = (this.sketch.noise(noiseI * angleNoiseScale, noiseJ * angleNoiseScale, noiseTime) * this.sketch.PI * 4) - this.sketch.TWO_PI;
        const magnitude = this.sketch.noise(noiseI * magnitudeNoiseScale, noiseJ * magnitudeNoiseScale, noiseTime)
        const flowVector = p5.Vector.fromAngle(angle, magnitude)

        field.push(flowVector)
      }
    }

    for (let i = 0; i < numParticles; i++) {
      particles.push(new Particle(this.sketch))
      // particles.push(new Particle(createVector(width/2, height)));
      // particles.push(new Particle(createVector(random(width), random(height))))
      // switch (i % 4) {
      //   case 0:
      //       particles.push(new Particle(createVector(random(width), 0)));
      //     break;
      //   case 1:
      //       particles.push(new Particle(createVector(random(width), height)));
      //     break;
      //   case 2:
      //       particles.push(new Particle(createVector(0, random(height))));
      //     break;
      //   case 3:
      //       particles.push(new Particle(createVector(width, random(height))));
      //     break;
      //   default: console.log('wut');
      // }
    }
    const configDiv = document.getElementById('config')

    respawnSelect = this.sketch.createSelect()
    this.addConfig(configDiv, respawnSelect, 'Respawn Select', 'select', ['random', 'side', 'mod frame'])
    respawnSelect.option('mod frame')
    respawnSelect.option('random')
    respawnSelect.option('side')
    respawnSelect.option('none')
    respawnSelect.changed(this.respawnSelectChanged.bind(this))

    respawnButton = this.sketch.createButton('Respawn')
    this.addConfig(configDiv, respawnButton, 'Respawn', 'button', this.clearBackground.bind(this))
    respawnButton.parent(configDiv)
    respawnButton.mouseClicked(() => {
      currentStep = 0
      this.clearBackground()
      for (const particle of particles) {
        particle.spawn()
      }
    })

    // this.sketch.frameRate(5);
  }

  setupUI () {
    this.configVue = new Vue({
      el: '#config',
      data: {
        currentStep: 0,
        fps: 0,
        params: [{
          name: 'Max Speed',
          value: maxSpeed,
          type: 'range',
          extra: {
            min: 0.0001, max: 10, step: 0.0001
          }
        }, {
          name: 'Angle Noise',
          value: angleNoiseScale,
          type: 'range',
          extra: {
            min: 0.0001, max: 0.1, step: 0.0001
          }
        }, {
          name: 'Magnitude Noise',
          value: magnitudeNoiseScale,
          type: 'range',
          extra: {
            min: 0.0001, max: 0.1, step: 0.0001
          }
        }, {
          name: 'Noise Time',
          value: noiseTimeScale,
          type: 'range',
          extra: {
            min: 0.0001, max: 0.1, step: 0.0001
          }
        }, {
          name: 'Show Flow',
          value: showFlow,
          type: 'checkbox'
        }]
      }
    })

    const clearButton = document.getElementById('clearBackground')
    clearButton.addEventListener('click', this.clearBackground.bind(this))
  }

  draw () {
    const config = this.paramsToObject()
    maxSpeed = config['Max Speed']
    angleNoiseScale = config['Angle Noise']
    magnitudeNoiseScale = config['Magnitude Noise']
    noiseTimeScale = config['Noise Time']
    const newShowFlow = config['Show Flow']
    if (newShowFlow != showFlow) {
      showFlow = newShowFlow
      this.showFlowChanged()
    }

    const scaling = spacing * 0.7
    noiseTime = currentStep * noiseTimeScale
    if (showFlow) {
      this.sketch.background(0)
    }

    // this.sketch.background(0)

    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        let magnitude = this.sketch.noise(i * magnitudeNoiseScale, j * magnitudeNoiseScale, noiseTime)
        const index = (j * rows) + i
        let angle = this.sketch.lerp(-this.sketch.TWO_PI, this.sketch.TWO_PI, this.sketch.noise((i + noiseOffset) * angleNoiseScale, (j + noiseOffset) * angleNoiseScale, noiseTime))
        // if (j === Math.floor(rows/2)) {
        //   angle = this.sketch.PI/2;
        //   magnitude *= 0.1;
        // } else if (j === Math.floor(rows/2) - 1) {
        //   angle = -this.sketch.PI/2;
        //   magnitude *= 0.1;
        if (xEffect) {
          if (i === j) {
            angle = -this.sketch.HALF_PI
            magnitude *= 2
          } else if (i === rows - j) {
            angle = this.sketch.HALF_PI
            magnitude *= 2
          } else {
            magnitude *= 0.1
          }
        }
        field[index] = p5.Vector.fromAngle(angle, magnitude)
      }
    }

    if (showFlow) {
      for (let j = 0; j < rows; j++) {
        for (let i = 0; i < cols; i++) {
          this.sketch.push()
          this.sketch.stroke(255)
          this.sketch.translate(i * spacing, j * spacing)
          const index = j * rows + i
          const flowVector = field[index]
          this.sketch.line(0, 0, flowVector.x * scaling, flowVector.y * scaling)
          this.sketch.noStroke()
          this.sketch.fill(200, 100, 150)
          this.sketch.ellipse(0, 0, 1)
          this.sketch.pop()
        }
      }
    }
    for (const particle of particles) {
      const flowIndex = Math.floor(particle.pos.x / spacing) + Math.floor((particle.pos.y / spacing)) * rows
      particle.applyForce(field[flowIndex])
      particle.update()
      particle.show()
    }
    // if (currentStep % 3000 === 0) {
    //   this.sketch.noStroke()
    //   this.sketch.fill(bgGrayScale, bgGrayScale, bgGrayScale, 3)
    //   this.sketch.rect(0, 0, this.sketch.width, this.sketch.height)
    // }
    // if (currentStep % 1000 === 0) {
    //   noStroke()
    //   fill(0, 0, 0);
    //   rect(0, 0, this.sketch.width, this.sketch.height);
    // }
    // noLoop()

    this.configVue.currentStep = currentStep
    this.configVue.fps = this.sketch.frameRate()

    currentStep++
  }

  paramsToObject () {
    const paramObj = {}

    for (const param of this.configVue.params) {
      paramObj[param.name] = param.value
    }

    return paramObj
  }

  clearBackground () {
    this.sketch.background(bgGrayScale)
  }

  showFlowChanged () {
    this.sketch.background(bgGrayScale)
  }

  respawnSelectChanged () {
    respawnType = respawnSelect.value()
  }

  addConfig (configDiv, element, name) {
    if (name !== undefined) {
      const label = document.createElement('label')
      label.textContent = `${name}:`
      element.parent(label)
      configDiv.appendChild(label)
    } else {
      element.parent(configDiv)
    }
  }
}

class Particle {
  constructor (sketch) {
    this.sketch = sketch
    this.pos = this.sketch.createVector(0, 0)
    // this.vel = this.sketch.createVector(this.sketch.random(-1, 1), this.sketch.random(-1, 1)).mult(5);
    this.vel = this.sketch.createVector(0, 0)
    this.acc = this.sketch.createVector(0, 0)
    this.trail = []
    this.last = null
    this.visible = true
    const biggestSide = Math.max(this.sketch.width, this.sketch.height)
    this.maxAge = Math.floor(this.sketch.random(biggestSide / 100, biggestSide / 2))
    this.age = Math.floor(this.sketch.random(0, this.maxAge))
    this.spawn()
  }

  spawn () {
    this.last = null
    this.age = 0
    this.visible = true
    switch (respawnType) {
      case 'random':
        var value = this.sketch.createVector(this.sketch.random(this.sketch.width), this.sketch.random(this.sketch.height))
        this.vel.mult(0.1)
        // console.log('rando: ', value);
        this.pos = value
        break
      case 'side':
        switch (this.maxAge % 4) {
          case 0:
            this.pos = this.sketch.createVector(this.sketch.random(this.sketch.width), 0)
            break
          case 1:
            this.pos = this.sketch.createVector(this.sketch.random(this.sketch.width), this.sketch.height)
            break
          case 2:
            this.pos = this.sketch.createVector(0, this.sketch.random(this.sketch.height))
            break
          case 3:
            this.pos = this.sketch.createVector(this.sketch.width, this.sketch.random(this.sketch.height))
            break
          default: console.log('wut')
        }
        break
      case 'mod frame':
        var multiplier = 4
        var place = currentStep * multiplier
        switch (this.maxAge % 4) {
          case 0:
            this.pos = this.sketch.createVector(place % this.sketch.width, 0)
            break
          case 1:
            this.pos = this.sketch.createVector(place % this.sketch.width, this.sketch.height)
            break
          case 2:
            this.pos = this.sketch.createVector(0, place % this.sketch.height)
            break
          case 3:
            this.pos = this.sketch.createVector(this.sketch.width, place % this.sketch.height)
            break
          default: console.log('wut')
        }
        break
      case 'none':
        this.visible = false
        break
      default:
        console.log('Wut: ', respawnType)
    }
  }

  update () {
    this.age += 1
    this.last = this.pos.copy()
    this.vel.add(this.acc)
    this.vel.limit(maxSpeed)
    this.vel.rotate(this.sketch.random(-0.2, 0.2))
    this.pos.add(this.vel)
    this.acc.mult(0)
    let wrapped = (this.pos.x > this.sketch.width || this.pos.y > this.sketch.height || this.pos.x < 0 || this.pos.y < 0)
    if (this.pos.x > this.sketch.width) {
      wrapped = true
      // this.pos.x = 0;
      // this.pos.y = random(height);
    } else if (this.pos.x < 0) {
      wrapped = true
      // this.pos.x = width;
      // this.pos.y = random(height);
    }

    if (this.pos.y > this.sketch.height) {
      wrapped = true
      // this.pos.y = 0;
      // this.pos.x = random(width);
    } else if (this.pos.y < 0) {
      wrapped = true
      // this.pos.y = height;
      // this.pos.x = random(width);
    }

    if (wrapped || this.age > this.maxAge) {
      // this.vel.mult(-10);
      // this.pos = this.sketch.createVector(mouseX, mouseY);
      // this.last = null;
      // this.pos = this.sketch.createVector(random(width), random(height));
      // this.vel.mult(0);
      // this.vel.mult(-1);
      this.age = 0
      this.spawn()
    }
    // if (this.trail.length > 15) {
    //   this.trail.shift();
    // }
    // this.trail.push(this.pos.copy());
  }

  applyForce (force) {
    this.acc.add(force)
    this.acc.mult(2.0)
  }

  show () {
    if (!this.visible) {
      return
    }
    // noStroke();
    // stroke(0, 20, currentStep % 256, 5);
    // const red = this.vel.mag() * 128;
    // stroke(red, Math.min(abs(((currentStep + 256) % 512) - 256), red * 0.8), 20, (this.age / this.maxAge) * 10);
    this.sketch.colorMode(this.sketch.HSB)
    const hue = Math.floor((this.age / this.maxAge) * 256)
    const brightness = this.maxAge % 20 < 10 ? 255 : 0
    this.sketch.fill(hue, 140, brightness, 0.1)
    this.sketch.stroke(hue, 255, brightness, 0.3)
    // if (currentStep % 256 === 0) {
    //   console.log('reset');
    // }
    this.sketch.strokeWeight(1)
    if (this.last !== null) {
      this.sketch.line(this.pos.x, this.pos.y, this.last.x, this.pos.y)
    }
    // for (const t of this.trail) {
    //   this.sketch.ellipse(t.x, t.y, 3);
    // }
    // this.sketch.stroke(hue, 255, 255, 0.5)
    // this.sketch.ellipse(this.pos.x, this.pos.y, 3);
  }
}

const ff = new FlowField()
const myp5 = new p5(ff.init.bind(ff)) // eslint-disable-line new-cap
if (myp5) {
  console.log('loaded')
}
