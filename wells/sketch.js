'use strict'

let field = []
const particles = []
const numParticles = 30000
const spacing = 40
let noiseOffset
let angleNoiseScale = 0.08
let magnitudeNoiseScale = 0.08
let noiseTimeScale = 0.0001
let noiseTime = 0
let cols, rows
let showFlow = false
let maxSpeed = 1.0
let respawnSelect
let respawnType = 'random'
let respawnButton
let respawnOnceButton
const xEffect = false
const bgGrayScale = 0
let currentStep = 0

Vue.component('config-param', {
  props: ['param'],
  template: `<div>
  <label>{{ param.name }}
    <template v-if="param.type === 'range'">
      <input
        v-model="param.value"
        v-bind:type="param.type"
        v-bind:min="param.extra.min"
        v-bind:max="param.extra.max"
        v-bind:step="param.extra.step">
        {{ param.value }}
      </input>
    </template>
    <template v-else-if="param.type === 'select'">
      <select v-model="param.value">
        <option
          v-for="option in param.extra.options"
          v-bind:value="option">
            {{ option }}
          </option>
      </select>
    </template>
    <template v-else>
      <input
        v-model="param.value"
        v-bind:type="param.type">
      </input>
    </template>
  </label>
</div>`
})

class FlowField {
  constructor () {
    this.sketch = undefined
    this.configVue = undefined
  }

  init (sketch) {
    this.sketch = sketch

    this.sketch.setup = this.setup.bind(this)
    this.sketch.draw = this.draw.bind(this)
  }

  setup () {
    this.setupUI()

    const canvas = this.sketch.createCanvas(800, 800)
    canvas.parent(document.getElementById('drawing'))
    this.sketch.background(bgGrayScale)
    noiseOffset = 0// random(0.1, 0.8);
    cols = Math.floor(this.sketch.width / spacing)
    rows = Math.floor(this.sketch.height / spacing)
    field = new Array(cols * rows)

    for (let i = 0; i < numParticles; i++) {
      particles.push(new Particle(this.sketch))
    }
    const configDiv = document.getElementById('config')

    respawnButton = this.sketch.createButton('Respawn')
    this.addConfig(configDiv, respawnButton, 'Respawn', 'button', this.clearBackground.bind(this))
    respawnButton.parent(configDiv)
    respawnButton.mouseClicked(() => {
      currentStep = 0
      this.clearBackground()
      for (const particle of particles) {
        particle.spawn()
      }
    })

    respawnOnceButton = this.sketch.createButton('Respawn Once')
    this.addConfig(configDiv, respawnOnceButton, 'Respawn Once', 'button', this.clearBackground.bind(this))
    respawnOnceButton.parent(configDiv)
    respawnOnceButton.mouseClicked(() => {
      currentStep = 0
      this.clearBackground()
      for (const particle of particles) {
        particle.spawn()
      }
      respawnType = 'none'
      for (const param of this.configVue.params) {
        if (param.name === 'Respawn Select') {
          param.value = 'none'
        }
      }
    })
  }

  setupUI () {
    this.configVue = new Vue({
      el: 'content',
      data: {
        currentStep: 0,
        fps: 0,
        params: [{
          name: 'Max Speed',
          value: maxSpeed,
          type: 'range',
          extra: {
            min: 0.0001, max: 10, step: 0.0001
          }
        }, {
          name: 'Angle Noise',
          value: angleNoiseScale,
          type: 'range',
          extra: {
            min: 0.0001, max: 0.1, step: 0.0001
          }
        }, {
          name: 'Magnitude Noise',
          value: magnitudeNoiseScale,
          type: 'range',
          extra: {
            min: 0.0001, max: 0.1, step: 0.0001
          }
        }, {
          name: 'Noise Time',
          value: noiseTimeScale,
          type: 'range',
          extra: {
            min: 0.0001, max: 0.1, step: 0.0001
          }
        }, {
          name: 'Show Flow',
          value: showFlow,
          type: 'checkbox'
        }, {
          name: 'Respawn Select',
          value: respawnType,
          type: 'select',
          extra: {
            options: [
              'random',
              'side',
              'mod frame',
              'none'
            ]
          }
        }]
      }
    })

    const clearButton = document.getElementById('clearBackground')
    clearButton.addEventListener('click', this.clearBackground.bind(this))
  }

  draw () {
    this.updateConfig()
    this.updateFlowField()

    if (showFlow) {
      this.drawFlow()
    }

    this.updateAndDrawParticles()

    this.configVue.currentStep = currentStep
    this.configVue.fps = this.sketch.frameRate()

    currentStep++
  }

  drawFlow () {
    const scaling = spacing * 0.7
    const halfSpacing = spacing * 0.5
    this.sketch.background(0)
    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        this.sketch.push()
        this.sketch.stroke(255)
        this.sketch.fill(200, 100, 150)
        this.sketch.translate((i * spacing) + halfSpacing, (j * spacing) + halfSpacing)
        const index = j * rows + i
        const flowVector = p5.Vector.mult(field[index], scaling)
        const triVectorLeft = p5.Vector.mult(flowVector, 0.7)
        const triVectorRight = triVectorLeft.copy()
        triVectorLeft.rotate(-0.15);
        triVectorRight.rotate(0.15);
        this.sketch.triangle(flowVector.x, flowVector.y, triVectorLeft.x, triVectorLeft.y, triVectorRight.x, triVectorRight.y)
        this.sketch.line(0, 0, flowVector.x, flowVector.y)
        this.sketch.pop()
      }
    }
  }

  updateAndDrawParticles () {
    for (const particle of particles) {
      particle.update()
      particle.show()
    }
  }

  updateConfig () {
    const config = this.paramsToObject()
    maxSpeed = config['Max Speed']
    angleNoiseScale = config['Angle Noise']
    magnitudeNoiseScale = config['Magnitude Noise']
    noiseTimeScale = config['Noise Time']
    const newShowFlow = config['Show Flow']
    if (!newShowFlow && showFlow !== newShowFlow) {
      this.clearBackground()
    }
    showFlow = newShowFlow
    respawnType = config["Respawn Select"]
  }

  updateFlowField () {
    noiseTime = currentStep * noiseTimeScale

    for (let j = 0; j < rows; j++) {
      for (let i = 0; i < cols; i++) {
        let magnitude = this.sketch.noise(i * magnitudeNoiseScale, j * magnitudeNoiseScale, noiseTime)
        const index = (j * rows) + i
        let angle = this.sketch.lerp(-this.sketch.TWO_PI, this.sketch.TWO_PI, this.sketch.noise((i + noiseOffset) * angleNoiseScale, (j + noiseOffset) * angleNoiseScale, noiseTime))

        if (xEffect) {
          if (i === j && j < rows / 2) {
            angle = -this.sketch.QUARTER_PI * 3
            // magnitude *= 2
          } else if (i === j && j >= rows / 2) {
            angle = this.sketch.QUARTER_PI
            // magnitude *= 2
          } else if (cols - i === j + 1 && j < rows / 2) {
            angle = -this.sketch.QUARTER_PI
            // magnitude *= 2
          } else if (cols - i === j + 1 && j >= rows / 2) {
            angle = this.sketch.QUARTER_PI * 3
            // magnitude *= 2
          } else {
            // magnitude *= 0.3
          }
        }
        field[index] = p5.Vector.fromAngle(angle, magnitude)
      }
    }
  }

  paramsToObject () {
    const paramObj = {}

    for (const param of this.configVue.params) {
      paramObj[param.name] = param.value
    }

    return paramObj
  }

  clearBackground () {
    this.sketch.background(bgGrayScale)
  }

  respawnSelectChanged () {
    respawnType = respawnSelect.value()
  }

  addConfig (configDiv, element, name) {
    if (name !== undefined) {
      const label = document.createElement('label')
      label.textContent = `${name}:`
      element.parent(label)
      configDiv.appendChild(label)
    } else {
      element.parent(configDiv)
    }
  }
}

class Particle {
  constructor (sketch) {
    this.sketch = sketch
    this.pos = this.sketch.createVector(0, 0)
    this.vel = this.sketch.createVector(0, 0)
    this.acc = this.sketch.createVector(0, 0)
    this.last = null
    this.visible = true
    const biggestSide = Math.max(this.sketch.width, this.sketch.height)
    this.maxAge = Math.floor(this.sketch.random(biggestSide / 100, biggestSide))
    this.age = Math.floor(this.sketch.random(0, this.maxAge))
    this.spawn()
  }

  spawn () {
    this.last = null
    this.age = 0
    this.visible = true
    this.vel.mult(0)
    switch (respawnType) {
      case 'random':
        var value = this.sketch.createVector(this.sketch.random(this.sketch.width), this.sketch.random(this.sketch.height))
        this.pos = value
        break
      case 'side':
        switch (this.maxAge % 4) {
          case 0:
            this.pos = this.sketch.createVector(this.sketch.random(this.sketch.width), 0)
            break
          case 1:
            this.pos = this.sketch.createVector(this.sketch.random(this.sketch.width), this.sketch.height)
            break
          case 2:
            this.pos = this.sketch.createVector(0, this.sketch.random(this.sketch.height))
            break
          case 3:
            this.pos = this.sketch.createVector(this.sketch.width, this.sketch.random(this.sketch.height))
            break
          default: console.log('wut')
        }
        break
      case 'mod frame':
        var multiplier = 4
        var place = currentStep * multiplier
        switch (this.maxAge % 4) {
          case 0:
            this.pos = this.sketch.createVector(place % this.sketch.width, 0)
            break
          case 1:
            this.pos = this.sketch.createVector(place % this.sketch.width, this.sketch.height)
            break
          case 2:
            this.pos = this.sketch.createVector(0, place % this.sketch.height)
            break
          case 3:
            this.pos = this.sketch.createVector(this.sketch.width, place % this.sketch.height)
            break
          default: console.log('wut')
        }
        break
      case 'none':
        this.visible = false
        break
      default:
        console.log('Wut: ', respawnType)
    }
  }

  update () {
    this.age += 1
    this.last = this.pos.copy()
    const flowX = Math.floor(this.pos.x / spacing);
    const flowY = Math.floor(this.pos.y / spacing);
    this.flowIndex = flowX + (flowY * rows)
    // console.log(this.flowIndex);
    // console.log(flowX, flowY, this.flowIndex)
    const left = field[this.flowIndex - 1]
    const right = field[this.flowIndex + 1]
    const up = field[this.flowIndex - cols]
    const down = field[this.flowIndex + cols]
    this.applyForce(field[this.flowIndex])
    if (left) {
      this.applyForce(left.copy().mult(0.25));
    }
    if (right) {
      this.applyForce(right.copy().mult(0.25));
    }
    if (up) {
      this.applyForce(up.copy().mult(0.25));
    }
    if (down) {
      this.applyForce(down.copy().mult(0.25));
    }
    // if (this.vel.magSq() !== 0) {
    //   this.acc.limit(0.1)
    // }
    // this.vel.mult(0.8)
    this.vel.add(this.acc)
    this.vel.limit(maxSpeed)
    this.vel.rotate(this.sketch.random(-0.2, 0.2))
    this.pos.add(this.vel)
    this.acc.mult(0)
    this.vel.mult(0.8)
    let wrapped = (this.pos.x > this.sketch.width || this.pos.y > this.sketch.height || this.pos.x < 0 || this.pos.y < 0)
    if (this.pos.x > this.sketch.width) {
      wrapped = true
    } else if (this.pos.x < 0) {
      wrapped = true
    }

    if (this.pos.y > this.sketch.height) {
      wrapped = true
    } else if (this.pos.y < 0) {
      wrapped = true
    }

    if (wrapped || this.age > this.maxAge) {
      this.age = 0
      this.spawn()
    }
  }

  applyForce (force) {
    this.acc.add(force)
  }

  show () {
    if (!this.visible) {
      return
    }

    this.sketch.colorMode(this.sketch.HSB)
    const hue = Math.floor((this.age / this.maxAge) * 256)
    const brightness = this.maxAge % 20 < 10 ? 255 : 0
    this.sketch.fill(hue, 140, brightness, 0.1)
    this.sketch.stroke(hue, 255, brightness, 0.3)

    this.sketch.strokeWeight(1)
    if (this.last !== null) {
      this.sketch.line(this.pos.x, this.pos.y, this.last.x, this.pos.y)
    }
  }
}

const ff = new FlowField()
const myp5 = new p5(ff.init.bind(ff)) // eslint-disable-line new-cap
if (myp5) {
  console.log('loaded')
}
