module.exports = {
  env: {
    browser: true,
    es2020: true
  },
  extends: [
    'plugin:vue/essential',
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 11
  },
  plugins: [
    'vue'
  ],
  globals: {
    p5: 'readonly',
    Vue: 'readonly'
  },
  rules: {
  }
}
