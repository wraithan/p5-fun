class Cell {
    constructor(i, j) {
        this.f = 0;
        this.g = 0;
        this.h = 0;

        this.i = i;
        this.j = j;

        this.pos = createVector();
        this.parent = null;

        this.wall = random(1) < 0.2;
    }

    show(type, col) {
        noStroke();
        if (this.wall) {
            col = 255;
        }
        if (type === 'point') {
            fill(col);
            ellipse(this.i * w + halfW, this.j * w + halfW, w / 4, w / 4);
        } else if (type === 'box') {
            fill(col);
            rect(this.i * w, this.j * w, w - 1, w - 1);
        }
    }

    getNeighbors() {
        const neighbors = [];

        for (let offsetI = -1; offsetI <= 1; offsetI++) {
            for (let offsetJ = -1; offsetJ <= 1; offsetJ++) {
                if (offsetI === 0 && offsetJ === 0) {
                    continue;
                }
                const nI = this.i + offsetI;
                const nJ = this.j + offsetJ;
                if (nI < 0 || nI >= cols || nJ < 0 || nJ >= rows) {
                    continue;
                } 
                neighbors.push(grid[nI + nJ * cols]);
            }            
        }

        return neighbors;
    }
}