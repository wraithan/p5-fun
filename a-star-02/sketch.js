// f(n) = g(n) + h(n)
const w = 10;
const halfW = w / 2;
const grid = [];
let rows, cols;
const openSet = new Set();
const closedSet = new Set();
let start, end;
let path = [];

function setup() {
  createCanvas(800, 800);
  cols = width / w;
  rows = height / w;
  background(0);
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      grid.push(new Cell(i, j));
    }
  }
  start = grid[Math.floor(cols / 2) + Math.floor(rows / 2) * cols];
  start.wall = false;
  // end = grid[grid.length - 1];
  end = random(grid);
  for (const endNeighbor in end.getNeighbors()) {
    endNeighbor.wall = true;
  }
  end.wall = false;
  openSet.add(start);
  frameRate(5);
}

function heuristic(a, b) {
  let d = dist(a.i, a.j, b.i, b.j);
  // let d = abs(a.i-b.i) + abs(a.j - b.j);
  return d;
}

function draw() {
  background(0);
  if (openSet.size > 0) {
    let winner;
    let lowestF = Infinity;
    for (const cell of openSet.values()) {
      if (cell.f < lowestF) {
        winner = cell;
        lowestF = winner.f;
      }
    }
    path = [];
    let current = winner;
    while (current) {
      path.push(current);
      current = current.parent;
    }

    if (winner == end) {
      console.log('Found it!');
      openSet.clear();
      closedSet.add(winner);
      noLoop();
      // openSet = [];
    } else {
      openSet.delete(winner);
      closedSet.add(winner);

      let neighbors = winner.getNeighbors();
      for (const neighbor of neighbors) {
        if (closedSet.has(neighbor) || neighbor.wall) continue;

        const tempG = winner.g + dist(neighbor.i, neighbor.j, winner.i, winner.j);
        if (!openSet.has(neighbor) || neighbor.g < tempG) {
          neighbor.g = tempG;
          neighbor.h = heuristic(neighbor, end);
          neighbor.f = neighbor.g + neighbor.h;
          neighbor.parent = winner;
          openSet.add(neighbor)
        }
      }
    }
  } else {
    console.log("Done!")
    noLoop();
  }

  for (const cell of grid) {
    cell.show('box', 128)
  }

  for (const cell of openSet.values()) {
    cell.show('box', color(0, 200, 100))
  }
  for (const cell of closedSet.values()) {
    cell.show('box', color(cell.f, 0, 0))
    // cell.show('box', color(cell.g * 5, cell.h * 2, 0))
  }

  for (const cell of path) {
    cell.show('point', 128)
  }
  start.show('point', color(0, 0, 255))
  end.show('point', color(0, 255, 0))

}