// f(n) = g(n) + h(n)
const w = 20;
const halfW = w / 2;
const grid = [];
let rows, cols;
const openSet = new Set();
const closedSet = new Set();
let start, end;
let path = [];

function setup() {
  createCanvas(400, 400);
  cols = width / w;
  rows = height / w;
  background(0);
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      grid.push(new Cell(i, j));
    }
  }
  start = grid[0];
  end = grid[grid.length - 1];
  openSet.add(start);
  // put setup code here
}

function heuristic(a, b) {
  // let d = dist(a.i, a.j, b.i, b.j);
  let d = abs(a.i-b.i) + abs(a.j - b.j);
  return d;
}

function draw() {
  background(0);
  if (openSet.size > 0) {
    let winner;
    let lowestF = Infinity;
    for (const cell of openSet.values()) {
      if (cell.f < lowestF) {
        winner = cell;
        lowestF = winner.f;
      }
    }
    path = [];
    let current = winner;
    while (current) {
      path.push(current);
      current = current.parent;
    }

    if (winner == end) {
      console.log('Found it!');
      openSet.clear();
      // openSet = [];
    }
    openSet.delete(winner);
    closedSet.add(winner);

    let neighbors = winner.getNeighbors();
    for (const neighbor of neighbors) {
      if (closedSet.has(neighbor)) continue;

      const tempG = winner.g + 1;
      if (!openSet.has(neighbor) || neighbor.g < tempG) {
        neighbor.g = tempG;
        neighbor.h = heuristic(neighbor, end);
        neighbor.f = neighbor.g + neighbor.h;
        neighbor.parent = winner;
        openSet.add(neighbor)
      }
    }
  } else {
    console.log("Done!")
    noLoop();
  }

  for (const cell of grid) {
    cell.show('box', 128)
  }

  for (const cell of openSet.values()) {
    cell.show('box', color(0, 200, 100))
  }
  for (const cell of closedSet.values()) {
    cell.show('box', 0)
  }

  for (const cell of path) {
    cell.show('point', 128)
  }
  start.show('point', color(0, 0, 255))
  end.show('point', color(0, 255, 0))

}