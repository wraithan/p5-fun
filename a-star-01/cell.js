class Cell {
    constructor(i, j) {
        this.f = 0;
        this.g = 0;
        this.h = 0;

        this.i = i;
        this.j = j;

        this.pos = createVector();
        this.parent = null;
    }

    show(type, col) {
        noStroke();
        if (type === 'point') {
            fill(col);
            ellipse(this.i * w + halfW, this.j * w + halfW, w / 4, w / 4);
        } else if (type === 'box') {
            fill(col);
            rect(this.i * w, this.j * w, w - 1, w - 1);
        }
    }

    getNeighbors() {
        const neighbors = [];

        if (this.i > 0) {
            neighbors.push(grid[(this.i - 1) + this.j * cols]);
        }
        if (this.i < (cols - 1)) {
            neighbors.push(grid[(this.i + 1) + this.j * cols]);
        }
        if (this.j > 0) {
            neighbors.push(grid[this.i + (this.j - 1) * cols]);
        }
        if (this.j < (rows - 1)) {
            neighbors.push(grid[this.i + (this.j + 1) * cols]);
        }

        return neighbors;
    }
}