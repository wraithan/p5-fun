class Agent {
    constructor(pos, color) {
        // this.pos = createVector(width / 2, height / 2);
        // this.pos = createVector(random(width), random(height));
        this.pos = pos;
        this.vel = createVector(0, 0);
        this.acc = createVector(0, 0);
        this.last = this.pos.copy();
        this.origin = this.pos.copy();
        this.color = color;
        this.maxSpeed = 3;
        this.maxForce = 0.1;
    }

    update() {
        this.last = this.pos.copy();
        this.vel.add(this.acc);
        this.vel.limit(this.maxSpeed);
        this.pos.add(this.vel);
        this.acc.mult(0);
        this.vel.rotate(random(-0.15, 0.15))
    }

    addForce(force) {
        this.acc.add(force);
    }

    seek(target) {
        const steering = target.copy();
        steering.sub(this.pos);
        steering.setMag(this.maxSpeed);
        steering.sub(this.vel);
        steering.limit(this.maxForce);
        this.addForce(steering);
    }

    show(alpha) {
        colorMode(HSB)
        strokeWeight(2);
        stroke(this.color, 255, 255, alpha);
        line(this.pos.x, this.pos.y, this.last.x, this.last.y);

        // strokeWeight(1);
        // stroke(this.color, 255, 255, alpha * 0.1);
        // line(this.pos.x, this.pos.y, this.origin.x, this.origin.y);
        // fill(this.color, 255, 255, alpha);
        // ellipse(this.pos.x, this.pos.y, 10, 10);
        colorMode(RGB)
    }
}