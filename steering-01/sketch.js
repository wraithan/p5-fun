
const numAgents = 15;
const agents = []
const numTargets = 200;
const targets = [];
let targetIndex = 0;
const iterations = 100;

function setup() {
  createCanvas(600, 600);
  background(0);

  for (let i = 0; i < numTargets; i++) {
    targets.push(createVector(random(width), random(height)))
  }

  // for (let i = 0; i < numTargets; i++) {
  //   let x, y;
  //   switch (Math.floor(random(3))) {
  //     case 0:
  //       x = 0;
  //       break;
  //     case 1:
  //       x = width;
  //       break;
  //     default:
  //       x = random(width);
  //   }
  //   if (i % 2 === 0) {
  //     y = height;
  //   } else {
  //     y = height - random(height / 4);
  //   }
  //   targets.push(createVector(x, y));
  // }
  // // Corners
  // const xOffset = width / 4;
  // const yOffset = height / 4;
  // for (let i = 0; i < numAgents; i++) {
  //   const color = Math.floor(random(256));
  //   let pos;
  //   // switch (Math.floor(random(4))) {
  //   switch (i % 4) {
  //     case 0:
  //       pos = createVector(xOffset, yOffset)
  //       break;
  //     case 1:
  //       pos = createVector(width - xOffset, yOffset);
  //       break;
  //     case 2:
  //       pos = createVector(xOffset, height - yOffset);
  //       break;
  //     case 3:
  //       pos = createVector(width - xOffset, height - yOffset);
  //       break;
  //   }
  //   agents.push(new Agent(pos, color));
  // }

  // Along the top
  const xOffset = width / numAgents;
  const yOffset = height / 5;
  for (let i = 0; i < numAgents; i++) {
    const color = Math.floor(random(256));
    // let pos = createVector(xOffset * i + (xOffset * 0.5), yOffset)
    let pos = targets[i].copy()
    agents.push(new Agent(pos, color));
  }
}

function draw() {
  if (frameCount < 2) {
    background(0);
  }
  noStroke()
  // for (let i = 0; i < numTargets; i++) {
  //   const target = targets[i];
  //   fill(128 + ((128 / numTargets) * i), 0, 0);
  //   ellipse(target.x, target.y, 5, 5);
  // }

  for (let i = 0; i < iterations; i++) { 
    const currentTarget = targets[targetIndex];
    let agent = agents[targetIndex % numAgents];
    agent.seek(currentTarget);
    agent.update();
    agent.show(0.1);
    if (agent.pos.dist(currentTarget) < 10) {
      targetIndex++;
      if (targetIndex == numTargets) {
        // noLoop()
        // console.log('Done');
        // return;
        targetIndex = targetIndex % numTargets;
      }
    }
  }
  console.log(targetIndex, numTargets);
}